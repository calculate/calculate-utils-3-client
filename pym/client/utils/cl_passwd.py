# -*- coding: utf-8 -*-

# Copyright 2013-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.utils.files import FilesError
from calculate.desktop.desktop import DesktopError
from ..client import ClientError
from calculate.lib.utils.samba import SambaError
from calculate.lib.cl_template import TemplatesError

_ = lambda x: x
setLocalTranslate('cl_client3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClPasswdAction(Action):
    """
    Изменить пароль доменного пользователя
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError, ClientError, DesktopError, TemplatesError,
                    SambaError)

    successMessage = (__("{cl_client_login}'s password changed\n"
                         "This modification will be applied when "
                         "you quit the X session"))
    failedMessage = __("Failed to change the password")
    interruptMessage = __("Password changing manually interrupted")

    # список задач для действия
    tasks = [
        {'name': 'change_passwd',
         'method': 'Client.clientPasswd(cl_client_login,ur_uid,ur_gid,'
                   'ur_home_path,ur_user_new_pw,ur_user_pw)',
         }]
