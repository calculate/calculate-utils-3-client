# -*- coding: utf-8 -*-

# Copyright 2013-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action, Tasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.utils.files import FilesError
from calculate.desktop.desktop import DesktopError
from ..client import ClientError
from calculate.lib.utils.samba import SambaError
from calculate.lib.cl_template import TemplatesError

_ = lambda x: x
setLocalTranslate('cl_client3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClClientAction(Action):
    """
    Ввести машину в домен или вывести
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError, ClientError, DesktopError, TemplatesError,
                    SambaError)

    successMessage = None
    failedMessage = None
    interruptMessage = __("Configuration manually interrupted")

    # список задач для действия
    tasks = [
        # команда вызвана только для монтировния remote ресурсов
        {'name': 'only_mount',
         'condition': lambda Get: Get('cl_client_mount_set') == 'on'
         },
        # ввод в домен
        {'name': 'domain',
         'condition': lambda Get: Get('cl_localhost_set') == 'off',
         'depend': Tasks.hasnot('only_mount')
         },
        # вывод из домена
        {'name': 'undomain',
         'condition': lambda Get: Get('cl_localhost_set') == 'on',
         'depend': Tasks.hasnot('only_mount')
         },
        # машина не доменная
        {'name': 'only_mount:localpc',
         'warning': __("This workstation is not in the domain"),
         'condition': lambda Get: not Get('cl_remote_host')
         },
        # проверить может ли указанный сервер являться доменом
        {'name': 'domain:check_domain',
         'message': __("Checking {cl_remote_host_new} for domain resources"),
         'method': 'Client.checkDomainServer(cl_remote_host_new,os_net_domain)',
         },
        # получить пароль для ввода в домен (или воспользоваться кэшированным)
        {'name': 'domain:get_password',
         'method': 'Client.getDomainPassword(cl_remote_host_new)'
         },
        {'name': 'domain:set_remote_host',
         'method': 'Client.setVariable("cl_remote_host",cl_remote_host_new)'
         },
        # машина доменная
        {'name': 'mount_remote',
         'method': 'Client.mountRemoteRes(cl_remote_pw,cl_client_remote_path,'
                   'cl_remote_host)',
         'depend': Tasks.success() & Tasks.hasnot('localpc', 'undomain')
         },
        # отключить удаленный доменный ресурс
        {'name': 'undomain:unmount_remote',
         'method': 'Client.umountSleepPath(cl_client_remote_path)',
         },
        # удалить переменные клиента
        {'name': 'undomain:remove_vars',
         'method': 'Client.removeVars()'
         },
        # наложить шаблоны если они не актуальны
        {'name': 'need_templates',
         'condition': lambda Get: Get('cl_client_relevance_set') == 'off',
         'depend': Tasks.success(inessential=['mount_remote'])
         },
        # проверить информацию для ldap расположенную в домене
        {'name': 'domain:check_domain_info',
         'method': 'Client.checkDomainInfo(cl_remote_host)',
         },
        # наложить доменные шаблоны, если успешно подключен удаленный ресурс
        {'name': 'need_templates:apply_templates',
         'message': __("Applying domain templates"),
         'method': 'Client.applyClientTemplates(cl_remote_host)',
         'depend': Tasks.success_all('mount_remote')
         },
        # обновить информацию для fastlogin-domain
        {'name': 'need_templates:update_fastlogin',
         'method': 'Client.update_fastlogin_domain_path()',
         'depend': Tasks.success_all('mount_remote')
         },
        # наложить недоменные шаблоны в случае локального режима
        # или были проблемы с подключением удаленноых ресурсов
        {'name': 'need_templates:apply_templates',
         'message': __("Applying non-domain templates"),
         'method': 'Client.applyClientTemplates("")',
         'depend': Tasks.result('mount_remote', ne=True)
         },
        # удалить записи из /etc/passwd и синхронизировать кэш
        {'name': 'del_sync_cache',
         'method': 'Client.cDelLdapSysUsersAndSyncCache()',
         'condition': lambda Get: Get('cl_remote_host'),
         'depend': Tasks.success_all('mount_remote', 'need_templates')
         },
        # удалить записи из /etc/passwd и очистить кэш
        {'name': 'undomain:del_clear_cache',
         'message': __("Clearing the user cache"),
         'method': 'Client.cDelLdapSysUsersAndClearCache()'
         },
        # синхронизировать кэш, добавить записи в /etc/passwd
        {'name': 'only_mount:add_sync_cache',
         'method': 'Client.cAddCacheUsersFromSystem()',
         'depend': Tasks.failed_all('mount_remote')
         },
        # удалить службу client из автозапуска
        {'name': 'undomain:noautorun_client',
         'method': 'Client.delDaemonAutostart("client")'
         },
        # добавить службу client в автозапуск
        {'name': 'domain:autorun_client',
         'method': 'Client.addDaemonAutostart("client")'
         },
        # записать переменные клиента
        {'name': 'domain:write_vars',
         'method': 'Client.writeClientVars(cl_remote_host,cl_ver,cl_remote_pw)',
         },
        # сообщения о результатах работы действия
        {'name': 'domain:success',
         'message': __("Workstation added to domain {cl_remote_host}")
         },
        {'name': 'domain:failed',
         'error': __(
             "Failed to add the workstation to domain {cl_remote_host}"),
         'depend': Tasks.failed() & Tasks.hasnot("interrupt"),
         },
        {'name': 'undomain:success',
         'message': __("Workstation removed from domain {cl_remote_host}")
         },
        {'name': 'undomain:failed',
         'error': __("Failed to remove the workstation from the domain"),
         'depend': Tasks.failed() & Tasks.hasnot("interrupt"),
         },
    ]
