# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from __future__ import absolute_import
import sys

from calculate.lib.datavars import VariableError, DataVarsError
from calculate.core.server.func import WsdlBase
from calculate.desktop.desktop import DesktopError
from .client import ClientError
from calculate.lib.utils.samba import SambaError
from .utils.cl_client import ClClientAction
from .utils.cl_passwd import ClPasswdAction
from .utils.cl_client_sync import (ClClientSyncLoginAction,
                                  ClClientSyncLogoutAction)
import calculate.desktop.desktop as desktop
from . import client

from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate

_ = lambda x: x
setLocalTranslate('cl_client3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class Wsdl(WsdlBase):
    methods = [
        #
        # ввести машину в домен или вывести
        #
        {
            # идентификатор метода
            'method_name': "client",
            # категория метода
            'category': __('Client'),
            # заголовок метода
            'title': __("Domain"),
            # иконка для графической консоли
            'image': 'calculate-client,network-server,network-workgroup',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-client',
            # права для запуска метода
            'rights': ['domain'],
            # объект содержащий модули для действия
            'logic': {'Desktop': desktop.Desktop,
                      'Client': client.Client},
            # описание действия
            'action': ClClientAction,
            # объект переменных
            'datavars': "client",
            'native_error': (VariableError, DataVarsError,
                             SambaError,
                             ClientError, DesktopError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'domain',
                        },
            'guivars': {'cl_localhost_set!': lambda dv: (
                "on" if dv.Get('cl_remote_host') == '' else 'off')},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Domain"),
                                    normal=(
                                        'cl_localhost_set',
                                        'cl_remote_host_new',
                                        'cl_remote_pw'),
                                    expert=('cl_client_mount_set',
                                            'cl_templates_locate',
                                            'cl_verbose_set',
                                            'cl_dispatch_conf'),
                                    next_label=_("Execute"))]},
        #
        # подключить удаленные ресурсы пользователя
        #
        {
            # идентификатор метода
            'method_name': "client_sync_login",
            # категория метода
            'category': __('Client'),
            # заголовок метода
            'title': __("Domain User Login"),
            # иконка для графической консоли
            'image': 'calculate-client-sync-login,application-other',
            # метод не присутствует в графической консоли
            'gui': False,
            # консольная команда
            'command': 'cl-client-sync-login',
            # права для запуска метода
            'rights': ['domainuser'],
            # объект содержащий модули для действия
            'logic': {'Desktop': desktop.Desktop,
                      'Client': client.Client},
            # описание действия
            'action': ClClientSyncLoginAction,
            # объект переменных
            'datavars': "client",
            'native_error': (VariableError, DataVarsError,
                             SambaError,
                             ClientError, DesktopError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'login'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Domain user login"),
                                    normal=('ur_login', 'cl_client_sync'),
                                    expert=('cl_client_ignore_errors_set',),
                                    next_label=_("Execute"))]},
        #
        # отключить удаленные ресурсы пользователя
        #
        {
            # идентификатор метода
            'method_name': "client_sync_logout",
            # категория метода
            'category': __('Client'),
            # заголовок метода
            'title': __("Domain User Logout"),
            # иконка для графической консоли
            'image': 'calculate-client-sync-logout,application-other',
            # метод не присутствует в графической консоли
            'gui': False,
            # консольная команда
            'command': 'cl-client-sync-logout',
            # права для запуска метода
            'rights': ['domainuser'],
            # объект содержащий модули для действия
            'logic': {'Desktop': desktop.Desktop,
                      'Client': client.Client},
            # описание действия
            'action': ClClientSyncLogoutAction,
            # объект переменных
            'datavars': "client",
            'native_error': (VariableError, DataVarsError,
                             SambaError,
                             ClientError, DesktopError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'logout'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Domain user logout"),
                                    normal=('ur_login', 'cl_client_sync'),
                                    next_label=_("Execute"))]},
        #
        # сменить пароль доменного пользователя
        #
        {
            # идентификатор метода
            'method_name': "client_passwd",
            # категория метода
            'category': __('Client'),
            # заголовок метода
            'title': __("Password Modification"),
            # иконка для графической консоли
            'image': 'calculate-client-passwd,preferences-system-privacy,system-users',
            # метод присутствует в графической консоли
            'gui': True,
            # пользовательский метода
            'user': True,
            # консольная команда
            'command': 'cl-passwd',
            # права для запуска метода
            'rights': ['password'],
            # объект содержащий модули для действия
            'logic': {'Desktop': desktop.Desktop,
                      'Client': client.Client},
            # описание действия
            'action': ClPasswdAction,
            # объект переменных
            'datavars': "client",
            'native_error': (VariableError, DataVarsError,
                             SambaError,
                             ClientError, DesktopError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'passwd'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Password modification"),
                                    normal=('cl_client_login', 'ur_user_pw',
                                            'ur_user_new_pw'),
                                    next_label=_("Save"))]},
    ]
