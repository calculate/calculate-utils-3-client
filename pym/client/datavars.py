# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

__app__ = 'calculate-client'
__version__ = '3.4.2'

import sys
from calculate.lib.datavars import DataVars

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_client3', sys.modules[__name__])


class DataVarsClient(DataVars):
    """Variable class for client package"""

    def importClient(self, **args):
        """Import client variables"""
        self.importVariables()
        self.importVariables('calculate.client.variables')
        self.defaultModule = "client"
