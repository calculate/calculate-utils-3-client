#!/usr/bin/env python
# -*- coding: utf-8 -*-

# setup.py --- Setup script for calculate-desktop

# Copyright 2012-2021 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

__app__ = "calculate-client"
__version__ = "3.2.2"

import os
from glob import glob
from distutils.core import setup, Extension
from distutils.command import install_data as module_install_data
from distutils.util import change_root, convert_path


data_files = [('/etc/init.d', [('data/client',0o755)]),
              ('/usr/share/calculate/xdm/login.d',
                            ['data/login.d/10client']),
              ('/usr/share/calculate/xdm/logout.d',
                            ['data/logout.d/10client']),
              ('/usr/share/calculate/xdm/',[('data/wait_domain',0o755)])]

class install_data(module_install_data.install_data):
    def run (self):
        self.mkpath(self.install_dir)
        for f in self.data_files:
            if isinstance(f, str):
                # it's a simple file, so copy it
                f = convert_path(f)
                if self.warn_dir:
                    self.warn("setup script did not provide a directory for "
                              "'%s' -- installing right in '%s'" %
                              (f, self.install_dir))
                (out, _) = self.copy_file(f, self.install_dir)
                self.outfiles.append(out)
            else:
                # it's a tuple with path to install to and a list of files
                dir = convert_path(f[0])
                if not os.path.isabs(dir):
                    dir = os.path.join(self.install_dir, dir)
                elif self.root:
                    dir = change_root(self.root, dir)
                self.mkpath(dir)

                if f[1] == []:
                    # If there are no files listed, the user must be
                    # trying to create an empty directory, so add the
                    # directory to the list of output files.
                    self.outfiles.append(dir)
                else:
                    # Copy files, adding them to the list of output files.
                    for data in f[1]:
                        # is's a simple filename without chmod
                        if isinstance(data,str):
                            chmod = None
                        else:
                            data, chmod = data
                        data = convert_path(data)
                        (out, _) = self.copy_file(data, dir)
                        if chmod and os.stat(out).st_mode != chmod:
                            os.chmod(out,chmod)
                        self.outfiles.append(out)

packages = [
    "calculate."+str('.'.join(root.split(os.sep)[1:]))
    for root, dirs, files in os.walk('pym/client')
    if '__init__.py' in files
]

setup(
    name = __app__,
    version = __version__,
    description = "Mounting resources and synchronize the user profile",
    author = "Calculate Ltd.",
    author_email = "support@calculate.ru",
    url = "http://calculate-linux.org",
    license = "http://www.apache.org/licenses/LICENSE-2.0",
    package_dir = {'calculate.client': "pym/client"},
    packages = packages,
    data_files = data_files,
    scripts=(glob('bin/*')),
    cmdclass={'install_data': install_data})
